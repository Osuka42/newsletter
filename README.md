### newsletter

How to run:
```bash
git clone https://gitlab.com/Osuka42/newsletter
cd Osuka42/newsletter

docker build --tag newsletter:0.0 .
docker run --publish 443:443 --detach --name newsletter newsletter:0.0
```

### Based on articles
https://docs.docker.com/get-started/part2/
https://blog.golang.org/docker