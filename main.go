package main

import (
	"net/http"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"golang.org/x/crypto/acme/autocert"
)

func main() {
	e := echo.New()

	e.AutoTLSManager.Cache = autocert.DirCache("/var/www/.cache")
	e.Use(middleware.Recover())
	e.Use(middleware.Logger())

	type greeting struct {
		Msg string `json:"message"`
	}

	e.GET("/", func(c echo.Context) error {
		return c.JSON(http.StatusOK, greeting{
			Msg: "Hello, World!",
		})
	})
	e.Logger.Fatal(e.StartAutoTLS(":443"))
}
