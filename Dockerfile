FROM golang:1.14

WORKDIR /app/go-sample-app

COPY go.mod .
COPY go.sum .

RUN go mod download

COPY . .

RUN go build -o ./out/newsletter .

EXPOSE 443

# Run the binary program produced by `go install`
CMD ["./out/newsletter"]